/**
 * 登录
 */
export const login = '/exam/miniapp/login';

/**
 * 账号注册
 */
export const register = '/exam/miniapp/register';

/**
 * 获取验证码
 */
export const sendCode = '/exam/common/sms/sendCode';

/**
 * 重置密码
 */
export const resetPwd = '/exam/miniapp/resetPwd';

/**
 * 模拟考试
 */
export const mockExam = '/exam/miniapp/mockExam';

/**
 * 正式考试
 */
export const examDesc = '/exam/miniapp/exam/';