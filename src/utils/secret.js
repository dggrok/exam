const CryptoJS = require('crypto-js')
const Base64 = require('crypto-js/enc-base64')

const key = CryptoJS.enc.Utf8.parse("cpcVEFy3rP4KwIk8");  //密钥
    
//解密方法
function Decrypt(word) {
    let decrypt = CryptoJS.AES.decrypt(word, key, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 });
    let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
    return decryptedStr.toString();
}

//加密方法
function Encrypt(word) {
    let srcs = CryptoJS.enc.Utf8.parse(word);
    let encrypted = CryptoJS.AES.encrypt(srcs, key, {  mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 });
    return encrypted.ciphertext.toString(CryptoJS.enc.Base64);
}

module.exports = {
    Decrypt :Decrypt,
    Encrypt :Encrypt
}
