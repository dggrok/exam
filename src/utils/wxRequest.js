import wepy from '@wepy/core';
// import util from './util';
// import md5 from './md5';
import tip from './tip';

// const API_SECRET_KEY = 'www.mall.cycle.com';
// const TIMESTAMP = util.getCurrentTime();
// const SIGN = md5.hex_md5((TIMESTAMP + API_SECRET_KEY).toLowerCase());

const wxRequest = async (params = {}, url, showLoading = false) => {
  if (showLoading) {
    tip.loading();
  }
  let data = params.query || {};
  //   data.sign = SIGN;
  //   data.time = TIMESTAMP;
  let res = await wepy.request({
    url: url,
    method: params.method || 'POST',
    data: data,
    header: { 'Content-Type': 'application/json' }
  });
  if (showLoading) {
    tip.loaded();
  }
  console.log(res, 'res');
  return res;
};
module.exports = {
  wxRequest
};
