export const baseUrl = 'https://hzlx.exam.legalboot.com';
// export const baseUrl = 'http://172.16.14.131:8901'

/**
 * 统一的请求封装
 * @param {String} api 请求的api地址
 * @param {JSON} params 请求携带的参数
 * @param {String} methods 请求方式，默认GET
 * @param {boolean} [loading=true] 是否显示loading，默认true
 */
export function appRequest(
  api,
  params,
  method = 'POST',
  loading = true,
  header = {}
) {
  return new Promise((resolve, reject) => {
    // 请求开始 loading
    if (loading) {
      wx.showLoading({
        title: '加载中,请稍后',
        mask: true
      })
    }
    // 请求
    wx.request({
      url: baseUrl + api,
      data: params,
      header: {
        ...header
      },
      method: method,
      dataType: 'json',
      success: function(res) {
        if (res.statusCode === 200) {
          resolve(res.data) // 成功之后的回调
          wx.hideLoading() // 结束加载
        } else {
          wx.hideLoading()
          reject()
          if (res.data.data.errMsg === 'token已过期请重新登陆') {
            setTimeout(() => {
              wx.reLaunch({
                url: '/pages/index'
              })
            }, 2000)
          }
        }
      },
      fail: function(error) {
        wx.hideLoading()
        reject(error)
        wx.showToast({
          title: '网络请求异常,请稍后重试！',
          icon: 'none',
          duration: 2000
        })
      }
    })
  })
}
