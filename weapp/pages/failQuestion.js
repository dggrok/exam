"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _request = require('./../utils/request.js');

var _api = require('./../api/api.js');

var _utils = _interopRequireDefault(require('./../utils/utils.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// 引入请求函数
var _require = require('./../iview/base/index.js'),
    $Toast = _require.$Toast;

_core["default"].page({
  // 选项
  data: {
    questionList: [],
    //问题列表
    currentNum: 1,
    //当前题号
    btnList: [{
      name: '取消'
    }],
    //弹窗按钮组
    currentAnswer: '',
    //当前答案
    rightAnswer: '' //正确答案

  },
  methods: {
    //获取数据
    getMockData: function getMockData() {
      var questionList = wx.getStorageSync('question') || [];
      this.questionList = this.filter(questionList);

      if (this.questionList && this.questionList.length != 0) {
        var currentList = [];

        for (var k = 0; k < this.questionList[0].options.length; k++) {
          var option = this.questionList[0].options[k];

          if (option.isSelect) {
            currentList.push(option.key);
          }
        }

        this.currentAnswer = currentList.join(',');
        this.rightAnswer = this.questionList[0].rightAnswer && this.questionList[0].rightAnswer.join(',');
      }
    },
    //下一题
    next: function next() {
      this.currentNum += 1;
      var question = this.questionList[this.currentNum - 1];
      var currentList = [];

      for (var i = 0; i < question.options.length; i++) {
        var option = question.options[i];

        if (option.isSelect) {
          currentList.push(option.key);
        }
      }

      this.currentAnswer = currentList.join(',');
      this.rightAnswer = question.rightAnswer && question.rightAnswer.join(',');
    },
    //过滤数组
    filter: function filter() {
      var arr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var resultList = [];

      for (var i = 0; i < arr.length; i++) {
        var currentList = [];

        for (var j = 0; j < arr[i].options.length; j++) {
          var option = arr[i].options[j];

          if (option.isSelect) {
            currentList.push(option.key);
          }
        }

        if (!_utils["default"].isEquar(currentList, arr[i].rightAnswer)) {
          resultList.push(arr[i]);
        }
      }

      return resultList;
    },
    //返回
    back: function back() {
      wx.navigateBack({
        delta: 1
      });
      wx.removeStorage('question');
    }
  },
  onLoad: function onLoad(options) {
    this.getMockData();
  },
  onUnload: function onUnload() {
    wx.removeStorage('question');
  }
}, {info: {"components":{"i-button":{"path":"..\\iview\\button\\index"},"i-row":{"path":"..\\iview\\row\\index"},"i-col":{"path":"..\\iview\\col\\index"},"i-modal":{"path":"..\\iview\\modal\\index"},"i-toast":{"path":"..\\iview\\toast\\index"},"i-spin":{"path":"..\\iview\\spin\\index"}},"on":{"13-0":["tap"],"13-1":["tap"]}}, handlers: {'13-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.next($event);
      })();
    
  }},'13-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.back($event);
      })();
    
  }}}, models: {}, refs: undefined });