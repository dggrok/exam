"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _request = require('./../utils/request.js');

var _api = require('./../api/api.js');

var _utils = _interopRequireDefault(require('./../utils/utils.js'));

var _constant = require('./../utils/constant.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// 引入请求函数
var _require = require('./../iview/base/index.js'),
    $Toast = _require.$Toast;

_core["default"].page({
  // 选项
  data: {
    height: '',
    //高度
    flag: 1,
    //1 模拟考试 2 正式考试
    desc: '',
    //考试细则,
    descAgreeTip: '已阅读并同意上述考试细则',
    disabled: true,
    checked: false
  },
  computed: {
    getHeight: function getHeight() {}
  },
  methods: {
    //是否同意按钮
    handleChange: function handleChange() {
      this.checked = !this.checked;
      this.disabled = !this.checked;
    },
    //获取模拟数据
    getMockData: function getMockData() {
      var that = this;
      var token = wx.getStorageSync('token') || {};
      (0, _request.appRequest)(_api.mockExam, {}, 'GET', true, {
        token: token.token
      }).then(function (res) {
        if (res.flag) {
          console.log(res, 'mockExam');
          var info = res.data || {};
          that.desc = info.desc;
        } else {
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 1,
            mask: false
          });
        }
      });
    },
    //获取正式考试考试须知
    getExamDesc: function getExamDesc() {
      var that = this;
      var token = wx.getStorageSync('token') || {};
      (0, _request.appRequest)(_api.examDesc + that.examId, {}, 'GET', true, {
        token: token.token
      }).then(function (res) {
        if (res.flag) {
          console.log(res, 'examDesc');
          var info = res.data || {};
          that.desc = info.desc;
        } else {
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 1,
            mask: false
          });
        }
      });
    },
    //模拟考试
    test: function test() {
      if (this.flag == 1) {
        wx.redirectTo({
          url: '/pages/exam'
        });
      } else {
        wx.redirectTo({
          url: '/pages/formal?examId=' + this.examId
        });
      }
    }
  },
  onLoad: function onLoad(options) {
    var systemInfo = wx.getStorageSync(_constant.SYSTEM_INFO) || {};
    this.height = systemInfo.screenHeight - 200;
    this.flag = options.id;

    if (this.flag == 2) {
      this.examId = wx.getStorageSync('examId') || {};
    }

    if (this.flag == 1) {
      this.getMockData();
    } else {
      this.getExamDesc();
    }
  }
}, {info: {"components":{"i-button":{"path":"..\\iview\\button\\index"},"i-row":{"path":"..\\iview\\row\\index"},"i-col":{"path":"..\\iview\\col\\index"},"i-modal":{"path":"..\\iview\\modal\\index"},"i-toast":{"path":"..\\iview\\toast\\index"},"i-spin":{"path":"..\\iview\\spin\\index"},"i-radio-group":{"path":"..\\iview\\radio-group\\index"},"i-radio":{"path":"..\\iview\\radio\\index"}},"on":{"9-0":["change"],"9-1":["tap"]}}, handlers: {'9-0': {"change": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.handleChange($event);
      })();
    
  }},'9-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.test($event);
      })();
    
  }}}, models: {}, refs: undefined });