"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _request = require('./../utils/request.js');

var _api = require('./../api/api.js');

var _utils = require('./../utils/utils.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// 引入请求函数
var _require = require('./../iview/base/index.js'),
    $Toast = _require.$Toast;

_core["default"].page({
  // 选项
  data: {
    questionList: [// {
      //     num:1,
      //     type:1,//题目类型 1 单选 2 多选 3 判断
      //     typeName:'单选题',
      //     topicContent:'社会治安综合治理的首要条件是 （）。',//题目内容
      //     options:[
      //         {
      //             key:'A',
      //             val:'调解矛盾纠纷',
      //             isSelect:false,
      //         },
      //         {
      //             key:'B',
      //             val:'依法严厉打击严重刑事犯罪活动',
      //             isSelect:false,
      //         },
      //         {
      //             key:'C',
      //             val:'加强治安防控体系建设',
      //             isSelect:false,
      //         },
      //         {
      //             key:'D',
      //             val:'10月1日',
      //             isSelect:false,
      //         }
      //     ],//选项
      // },
      // {
      //     num:2,
      //     type:2,//题目类型 1 单选 2 多选 3 判断
      //     typeName:'多选题',
      //     topicContent:'根据《预防未成年犯罪法》规定，未成年的父母或者其他监护人对未成年的法治教育负有 （）。',//题目内容
      //     options:[
      //         {
      //             key:'A',
      //             val:'间接责任',
      //             isSelect:false,
      //         },
      //         {
      //             key:'B',
      //             val:'直接责任',
      //             isSelect:false,
      //         },
      //         {
      //             key:'C',
      //             val:'监管责任',
      //             isSelect:false,
      //         },
      //         {
      //             key:'D',
      //             val:'无责任',
      //             isSelect:false,
      //         }
      //     ],//选项
      // },
      // {
      //     num:3,
      //     type:3,//题目类型 1 单选 2 多选 3 判断
      //     typeName:'判断题',
      //     topicContent:'各级各部门主要领导是平安建设和综治工作第二责任人，要亲自研究部署和协调工作中存在的困难和问题 （）。',//题目内容
      //     options:[
      //         {
      //             key:'A',
      //             val:'对',
      //             isSelect:false,
      //         },
      //         {
      //             key:'B',
      //             val:'错',
      //             isSelect:false,
      //         },
      //     ],//选项
      // }
    ],
    //问题列表
    currentNum: 1,
    //当前题号
    remainTime: "",
    timer: '',
    //倒计时
    disable: false,
    //倒计时结束不能答题
    btnList: [{
      name: '取消'
    }],
    //弹窗按钮组
    showModal: false,
    //显示提交弹窗
    showModal1: false,
    showTips1: '确定要直接交卷吗，',
    //提示语
    showTips2: '交卷后将直接结算当前成绩！',
    //提示语
    showTips3: ''
  },
  methods: {
    //获取考试题目
    getExamData: function getExamData() {
      var that = this;
      var token = wx.getStorageSync('token') || {};
      var examId = wx.getStorageSync('examId') || {};
      (0, _request.appRequest)(_api.examDesc + examId + '/question', {}, 'GET', true, {
        token: token.token
      }).then(function (res) {
        if (res.flag) {
          var info = res.data || {};
          that.questionList = that.translateData(info);
          that.remainTime = (0, _utils.formatSeconds)(info.singleTime || 60); //开始倒计时

          that.countDown(that, info.singleTime || 60);
        } else {
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 1,
            mask: false
          });
          setTimeout(function () {
            wx.navigateBack({
              delta: 1
            });
          }, 2000);
        }
      });
    },
    //转化数据格式
    translateData: function translateData(info) {
      var list = [];
      var singleList = info.singleQuestions || [];
      var multipleList = info.multipleQuestions || [];
      var judgeList = info.judgeQuestions || [];

      for (var i = 0; i < singleList.length; i++) {
        var item = singleList[i];
        var options = [];

        if (item.itemA) {
          options.push({
            key: "A",
            val: item.itemA,
            isSelect: false
          });
        }

        if (item.itemB) {
          options.push({
            key: "B",
            val: item.itemB,
            isSelect: false
          });
        }

        if (item.itemC) {
          options.push({
            key: "C",
            val: item.itemC,
            isSelect: false
          });
        }

        if (item.itemD) {
          options.push({
            key: "D",
            val: item.itemD,
            isSelect: false
          });
        }

        list.push({
          id: item.id,
          num: i + 1,
          type: 1,
          questionTime: info.singleTime || 60,
          typeName: '单选题',
          score: info.singleScore,
          topicContent: item.question,
          options: options
        });
      }

      for (var j = 0; j < multipleList.length; j++) {
        var _item = multipleList[j];
        var _options = [];

        if (_item.itemA) {
          _options.push({
            key: "A",
            val: _item.itemA,
            isSelect: false
          });
        }

        if (_item.itemB) {
          _options.push({
            key: "B",
            val: _item.itemB,
            isSelect: false
          });
        }

        if (_item.itemC) {
          _options.push({
            key: "C",
            val: _item.itemC,
            isSelect: false
          });
        }

        if (_item.itemD) {
          _options.push({
            key: "D",
            val: _item.itemD,
            isSelect: false
          });
        }

        list.push({
          id: _item.id,
          num: singleList.length + 1 + j,
          type: 2,
          typeName: '多选题',
          score: info.multipleScore,
          questionTime: info.multipleTime || 60,
          topicContent: _item.question,
          options: _options
        });
      }

      for (var k = 0; k < judgeList.length; k++) {
        var _item2 = judgeList[k];
        var _options2 = [];

        if (_item2.itemA) {
          _options2.push({
            key: "A",
            val: _item2.itemA,
            isSelect: false
          });
        }

        if (_item2.itemB) {
          _options2.push({
            key: "B",
            val: _item2.itemB,
            isSelect: false
          });
        }

        list.push({
          id: _item2.id,
          num: singleList.length + multipleList.length + 1 + k,
          type: 3,
          typeName: '判断题',
          score: info.judgeScore,
          questionTime: info.judgeTime || 60,
          topicContent: _item2.question,
          options: _options2
        });
      }

      return list;
    },
    //下一题
    next: function next() {
      var flag = false;
      var count = 0;

      for (var i = 0; i < this.questionList[this.currentNum - 1].options.length; i++) {
        if (this.questionList[this.currentNum - 1].options[i].isSelect) {
          flag = true;
          break;
        }
      }

      if (!flag && !this.disable) {
        this.showModal1 = true;
        this.showTips3 = '当前题目还未作答！';
        return;
      }

      if (this.questionList[this.currentNum - 1].type == 2) {
        for (var _i = 0; _i < this.questionList[this.currentNum - 1].options.length; _i++) {
          if (this.questionList[this.currentNum - 1].options[_i].isSelect) {
            count++;
          }
        }

        if (count <= 1 && !this.disable) {
          this.showModal1 = true;
          this.showTips3 = '当前题目为多选题，请至少选择两个选项！';
          return;
        }
      }

      this.currentNum += 1;
      if (this.timer) clearInterval(this.timer);
      this.remainTime = (0, _utils.formatSeconds)(this.questionList[this.currentNum - 1].questionTime || 60); //重新开始倒计时

      this.countDown(this, this.questionList[this.currentNum - 1].questionTime);
    },
    //倒计时
    countDown: function countDown(that, count) {
      that.disable = false;

      if (count <= 0) {
        that.disable = true;
        if (that.timer) clearInterval(that.timer);
        return;
      }

      that.timer = setInterval(function () {
        that.remainTime = (0, _utils.formatSeconds)(count);
        count--;

        if (count < 0) {
          that.disable = true;
          if (that.timer) clearInterval(that.timer);

          if (that.currentNum == that.questionList.length) {
            that.finish();
          } else {
            that.next();
          }

          return;
        }
      }, 1000);
    },
    //选择
    selectOption: function selectOption(item, option, index, index1) {
      var that = this; //type 1 单选 3 判断题

      if (item.type === 1 || item.type === 3) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = that.questionList[index1].options[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var question = _step.value;
            question.isSelect = false;
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
              _iterator["return"]();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }

        that.questionList[index1].options[index].isSelect = true;
      } //type 2 多选


      if (item.type === 2) {
        that.questionList[index1].options[index].isSelect = !that.questionList[index1].options[index].isSelect;
      }
    },
    //提交
    submit: function submit(flag) {
      this.showModal = true;
    },
    //关闭模态框
    cancelModal: function cancelModal() {
      this.showModal = false;
    },
    cancel: function cancel() {
      this.showModal1 = false;
    },
    //确认交卷
    finish: function finish() {
      var _this = this;

      this.showModal = false;
      var requestList = this.translateAnswer();
      var that = this;
      $Toast({
        content: '分数正在核算中',
        type: 'loading',
        duration: 0,
        mask: false
      });
      var token = wx.getStorageSync('token') || {};
      var examId = wx.getStorageSync('examId') || {};
      (0, _request.appRequest)(_api.examDesc + examId + '/answer', requestList, 'post', false, {
        token: token.token
      }).then(function (res) {
        if (that.timer) clearInterval(that.timer);

        if (res.flag) {
          $Toast.hide();
          var info = res.data || {};
          if (_this.timer) clearInterval(_this.timer); //正式考试成绩结算

          wx.redirectTo({
            url: '/pages/achievement?id=2&score=' + info.writtenScore + '&passScore=' + info.writtenPassScore
          });
        } else {
          $Toast.hide();
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 1,
            mask: false
          });
        }
      })["catch"](function (error) {
        $Toast.hide();
      });
    },
    // 转换提交答案
    translateAnswer: function translateAnswer() {
      var _this2 = this;

      var list = [];

      var _loop = function _loop(i) {
        var item = _this2.questionList[i];
        var selectedItem = item.options.filter(function (item1) {
          return item1.isSelect;
        });
        var selectedItems = [];
        selectedItem.forEach(function (item3) {
          selectedItems.push(item3.key);
        });
        list.push({
          id: item.id,
          answer: selectedItems.join(',')
        });
      };

      for (var i = 0; i < this.questionList.length; i++) {
        _loop(i);
      }

      return list;
    }
  },
  onLoad: function onLoad(options) {
    this.examId = options.examId;
  },
  onShow: function onShow() {
    this.getExamData(); //重置页面

    this.currentNum = 1;
    this.remainTime = "";
    this.timer = null;
    this.disable = false;
    this.showModal = false;
    this.showModal1 = false;
  },
  onHide: function onHide() {
    if (this.timer) clearInterval(this.timer);
  },
  onUnload: function onUnload() {
    if (this.timer) clearInterval(this.timer);
  }
}, {info: {"components":{"i-button":{"path":"..\\iview\\button\\index"},"i-row":{"path":"..\\iview\\row\\index"},"i-col":{"path":"..\\iview\\col\\index"},"i-modal":{"path":"..\\iview\\modal\\index"},"i-toast":{"path":"..\\iview\\toast\\index"},"i-spin":{"path":"..\\iview\\spin\\index"}},"on":{"12-2":["tap"],"12-3":["tap"],"12-4":["ok","cancel"],"12-6":["tap"]}}, handlers: {'12-0': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.submit(1);
      })();
    
  }},'12-1': {"tap": function proxy (item, option, index, index1) {
    
    var _vm=this;
      return (function () {
        _vm.selectOption(item,option,index,index1);
      })();
    
  }},'12-2': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.next($event);
      })();
    
  }},'12-3': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.submit(2);
      })();
    
  }},'12-4': {"ok": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.finish($event);
      })();
    
  }, "cancel": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.cancelModal($event);
      })();
    
  }},'12-6': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.cancel($event);
      })();
    
  }}}, models: {}, refs: undefined });