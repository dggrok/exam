"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _constant = require('./../utils/constant.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  // 选项
  data: {
    height: '',
    //高度
    num: 0,
    flag: 1,
    //1 模拟考试结算 2 正式考试结算
    writtenPassScore: 0,
    //考试合格分数
    showError: true //显示错题按钮

  },
  computed: {
    getHeight: function getHeight() {}
  },
  methods: {
    //查看错题
    view: function view() {
      wx.navigateTo({
        url: '/pages/failQuestion'
      });
    },
    //模拟考试
    exam: function exam() {
      wx.removeStorage('question');
      wx.redirectTo({
        url: '/pages/exam'
      });
    },
    //返回
    back: function back() {
      wx.navigateBack({
        delta: 1
      });
      wx.removeStorage('question');
    }
  },
  onLoad: function onLoad(options) {
    var systemInfo = wx.getStorageSync(_constant.SYSTEM_INFO) || {};
    var question = wx.getStorageSync('question') || [];
    var totalScore = 0;

    if (question && question.length !== 0) {
      for (var i = 0; i < question.length; i++) {
        totalScore += question[i].score;
      }

      if (totalScore == options.score) {
        this.showError = false;
      } else {
        this.showError = true;
      }
    }

    this.height = systemInfo.screenHeight - 300;
    this.flag = options.id;
    this.num = Number(options.score);
    this.writtenPassScore = Number(options.passScore) || 0;
  }
}, {info: {"components":{"i-button":{"path":"..\\iview\\button\\index"},"i-row":{"path":"..\\iview\\row\\index"},"i-col":{"path":"..\\iview\\col\\index"},"i-modal":{"path":"..\\iview\\modal\\index"}},"on":{"11-0":["tap"],"11-1":["tap"],"11-2":["tap"]}}, handlers: {'11-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.exam($event);
      })();
    
  }},'11-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.view($event);
      })();
    
  }},'11-2': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.back($event);
      })();
    
  }}}, models: {}, refs: undefined });