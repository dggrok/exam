"use strict";

var _regeneratorRuntime2 = _interopRequireDefault(require('./../vendor.js')(6));

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _store = _interopRequireDefault(require('./../store/index.js'));

var _constant = require('./../utils/constant.js');

var _request = require('./../utils/request.js');

var _api = require('./../api/api.js');

var _utils = _interopRequireDefault(require('./../utils/utils.js'));

var _md = _interopRequireDefault(require('./../utils/md5.js'));

var _tip = _interopRequireDefault(require('./../utils/tip.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('./../iview/base/index.js'),
    $Toast = _require.$Toast;

_core["default"].page({
  store: _store["default"],
  data: {
    phone: '',
    //手机号
    password: '',
    //密码
    showModal: false,
    //弹出窗显示
    showLoading: false,
    showTips: '',
    //提示语
    btnList: [{
      name: '取消'
    }]
  },
  computed: {},
  methods: {
    //登录
    handleClick: function handleClick() {
      var _this = this;

      if (!this.phone) {
        this.showModal = true;
        this.showTips = '手机号不能为空!';
        return;
      }

      if (!_utils["default"].vailPhone(this.phone)) {
        this.showModal = true;
        this.showTips = '手机号格式不正确!';
        return;
      }

      if (!this.password) {
        this.showModal = true;
        this.showTips = '密码不能为空!';
        return;
      }

      $Toast({
        content: '登录中',
        type: 'loading',
        duration: 0,
        mask: false
      });
      this.showLoading = true;
      var request = {
        username: this.phone,
        password: _md["default"].hex_md5(this.password)
      };
      (0, _request.appRequest)(_api.login, request, 'POST', false).then(function (res) {
        if (res.flag) {
          _this.showLoading = false;
          var tokenInfo = res.data;
          wx.setStorageSync('token', tokenInfo);
          $Toast.hide(); //跳转页面

          wx.navigateTo({
            url: '/pages/select'
          });
        } else {
          _this.showLoading = false;
          $Toast.hide();
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 1,
            mask: false
          });
        }
      })["catch"](function (error) {
        _this.showLoading = false;
        $Toast.hide();
      });
    },
    //账号注册
    register: function register() {
      wx.navigateTo({
        url: '/pages/register'
      }); //细则
      // wx.navigateTo({
      //   url: '/pages/testDetail'
      // })
      //模拟考试
      // wx.navigateTo({
      //   url: '/pages/exam'
      // })
      //模拟考试成绩结算
      // wx.navigateTo({
      //   url: '/pages/achievement'
      // })
    },
    //忘记密码
    forgetPassWd: function forgetPassWd() {
      wx.navigateTo({
        url: '/pages/forget'
      });
    },
    //关闭模态框
    cancelModal: function cancelModal() {
      console.log(111);
      this.showModal = false;
      this.showTips = '';
    }
  },
  onLoad: function () {
    var _onLoad = _asyncToGenerator(
    /*#__PURE__*/
    _regeneratorRuntime2["default"].mark(function _callee() {
      var res;
      return _regeneratorRuntime2["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return wx.getSystemInfoSync();

            case 2:
              res = _context.sent;
              wx.setStorageSync(_constant.SYSTEM_INFO, res);

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function onLoad() {
      return _onLoad.apply(this, arguments);
    }

    return onLoad;
  }()
}, {info: {"components":{"i-button":{"path":"..\\iview\\button\\index"},"i-row":{"path":"..\\iview\\row\\index"},"i-col":{"path":"..\\iview\\col\\index"},"i-modal":{"path":"..\\iview\\modal\\index"},"i-toast":{"path":"..\\iview\\toast\\index"},"i-spin":{"path":"..\\iview\\spin\\index"}},"on":{"5-0":["tap"],"5-3":["tap"]}}, handlers: {'5-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.handleClick($event);
      })();
    
  }},'5-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.register($event);
      })();
    
  }},'5-2': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.forgetPassWd($event);
      })();
    
  }},'5-3': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.cancelModal($event);
      })();
    
  }}}, models: {'0': {
      type: "input",
      expr: "phone",
      handler: function set ($v) {
      var _vm=this;
        _vm.phone = $v;
      
    }
    },'1': {
      type: "input",
      expr: "password",
      handler: function set ($v) {
      var _vm=this;
        _vm.password = $v;
      
    }
    }}, refs: undefined });