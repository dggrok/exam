"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _secret = _interopRequireDefault(require('./../utils/secret.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  // 选项
  data: {},
  methods: {
    //考试
    test: function test(flag) {
      console.log(flag, 'flag'); //模拟考试获取数据跳转

      if (flag === 1) {
        wx.navigateTo({
          url: '/pages/testDetail?id=' + flag
        });
      }

      if (flag === 2) {
        wx.scanCode({
          success: function success(res) {
            var word = res.result;

            var examId = _secret["default"].Decrypt(word);

            wx.setStorageSync('examId', examId);
            wx.navigateTo({
              url: '/pages/testDetail?id=2'
            });
          }
        });
      }
    }
  },
  onLoad: function onLoad() {}
}, {info: {"components":{"i-button":{"path":"..\\iview\\button\\index"},"i-row":{"path":"..\\iview\\row\\index"},"i-col":{"path":"..\\iview\\col\\index"},"i-modal":{"path":"..\\iview\\modal\\index"}},"on":{"8-0":["tap"],"8-1":["tap"]}}, handlers: {'8-0': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.test(1);
      })();
    
  }},'8-1': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.test(2);
      })();
    
  }}}, models: {}, refs: undefined });