"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _request = require('./../utils/request.js');

var _api = require('./../api/api.js');

var _utils = _interopRequireDefault(require('./../utils/utils.js'));

var _md = _interopRequireDefault(require('./../utils/md5.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// 引入请求函数
var _require = require('./../iview/base/index.js'),
    $Toast = _require.$Toast;

_core["default"].page({
  // 选项
  data: {
    userInfo: {
      phone: '',
      //手机号
      code: '',
      //验证码
      name: '',
      //真实姓名
      idCard: '',
      //身份证号
      password: '',
      //密码
      site: '' //律所

    },
    showModal: false,
    //弹出窗显示
    showLoading: false,
    showTips: '',
    //提示语
    btnList: [{
      name: '取消'
    }],
    timer: '',
    time: '60秒',
    showTime: false
  },
  methods: {
    //验证码倒计时
    countDown: function countDown(that, count) {
      if (count <= 0) {
        that.disable = true;
        if (that.timer) clearInterval(that.timer);
        return;
      }

      that.timer = setInterval(function () {
        that.time = count + '秒';
        count--;

        if (count < 0) {
          that.disable = true;
          if (that.timer) clearInterval(that.timer);
          return;
        }
      }, 1000);
    },
    //获取验证码
    getCode: function getCode() {
      var _this = this;

      if (!this.userInfo.phone) {
        this.showModal = true;
        this.showTips = '请先填写手机号!';
        return;
      }

      var request = {
        phone: this.userInfo.phone,
        type: 0
      };
      (0, _request.appRequest)(_api.sendCode, request, 'POST').then(function (res) {
        if (res.flag) {
          _this.showTime = true;

          _this.countDown(_this, 60);
        } else {
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 1,
            mask: false
          });
        }
      });
    },
    //提交
    submit: function submit() {
      var _this2 = this;

      if (!this.userInfo.phone) {
        this.showModal = true;
        this.showTips = '手机号不能为空!';
        return;
      }

      if (!_utils["default"].vailPhone(this.userInfo.phone)) {
        this.showModal = true;
        this.showTips = '手机号格式不正确!';
        return;
      }

      if (!this.userInfo.code) {
        this.showModal = true;
        this.showTips = '验证码不能为空!';
        return;
      }

      if (!this.userInfo.name) {
        this.showModal = true;
        this.showTips = '姓名不能为空!';
        return;
      }

      if (!this.userInfo.idCard) {
        this.showModal = true;
        this.showTips = '身份证号不能为空!';
        return;
      }

      if (!_utils["default"].valiIdCard(this.userInfo.idCard)) {
        this.showModal = true;
        this.showTips = '身份证号格式不正确!';
        return;
      }

      if (!this.userInfo.password) {
        this.showModal = true;
        this.showTips = '请设置密码!';
        return;
      }

      if (!this.userInfo.site) {
        this.showModal = true;
        this.showTips = '请填写您所在的律所或单位!';
        return;
      }

      $Toast({
        content: '注册中',
        type: 'loading',
        duration: 0,
        mask: false
      });
      this.showLoading = true;
      var request = {
        phone: this.userInfo.phone,
        code: this.userInfo.code,
        name: this.userInfo.name,
        idCard: this.userInfo.idCard,
        password: _md["default"].hex_md5(this.userInfo.password),
        lawFirm: this.userInfo.site
      };
      (0, _request.appRequest)(_api.register, request, 'POST', false).then(function (res) {
        if (res.flag) {
          $Toast.hide();
          _this2.showLoading = false;
          $Toast({
            content: '注册成功,即将跳转至登录页！',
            type: 'success',
            duration: 2,
            mask: false
          });
          setTimeout(function () {
            //跳转页面
            wx.redirectTo({
              url: '/pages/index'
            });
          }, 2000);
        } else {
          _this2.showLoading = false;
          $Toast.hide();
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 2,
            mask: false
          });
        }
      })["catch"](function (error) {
        $Toast.hide();
        _this2.showLoading = false;
      });
    },
    //关闭模态框
    cancelModal: function cancelModal() {
      this.showModal = false;
      this.showTips = '';
    }
  },
  created: function created() {},
  onUnload: function onUnload() {
    if (this.timer) clearInterval(this.timer);
  }
}, {info: {"components":{"i-button":{"path":"..\\iview\\button\\index"},"i-row":{"path":"..\\iview\\row\\index"},"i-col":{"path":"..\\iview\\col\\index"},"i-modal":{"path":"..\\iview\\modal\\index"},"i-toast":{"path":"..\\iview\\toast\\index"},"i-spin":{"path":"..\\iview\\spin\\index"}},"on":{"6-1":["tap"],"6-2":["tap"]}}, handlers: {'6-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.getCode($event);
      })();
    
  }},'6-1': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.submit($event);
      })();
    
  }},'6-2': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.cancelModal($event);
      })();
    
  }}}, models: {'2': {
      type: "input",
      expr: "userInfo.phone",
      handler: function set ($v) {
      var _vm=this;
        _vm.$set(_vm.userInfo, "phone", $v);
      
    }
    },'3': {
      type: "input",
      expr: "userInfo.name",
      handler: function set ($v) {
      var _vm=this;
        _vm.$set(_vm.userInfo, "name", $v);
      
    }
    },'4': {
      type: "input",
      expr: "userInfo.idCard",
      handler: function set ($v) {
      var _vm=this;
        _vm.$set(_vm.userInfo, "idCard", $v);
      
    }
    },'5': {
      type: "input",
      expr: "userInfo.password",
      handler: function set ($v) {
      var _vm=this;
        _vm.$set(_vm.userInfo, "password", $v);
      
    }
    },'6': {
      type: "input",
      expr: "userInfo.site",
      handler: function set ($v) {
      var _vm=this;
        _vm.$set(_vm.userInfo, "site", $v);
      
    }
    },'7': {
      type: "input",
      expr: "userInfo.code",
      handler: function set ($v) {
      var _vm=this;
        _vm.$set(_vm.userInfo, "code", $v);
      
    }
    }}, refs: undefined });