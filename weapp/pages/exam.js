"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _utils = _interopRequireWildcard(require('./../utils/utils.js'));

var _request = require('./../utils/request.js');

var _api = require('./../api/api.js');

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _require = require('./../iview/base/index.js'),
    $Toast = _require.$Toast;

_core["default"].page({
  // 选项
  data: {
    questionList: [],
    //问题列表
    currentNum: 1,
    //当前题号
    remainTime: "",
    timer: '',
    //倒计时
    disable: false,
    //倒计时结束不能答题
    btnList: [{
      name: '取消'
    }],
    //弹窗按钮组
    showModal: false,
    //显示提交弹窗
    showModal1: false,
    showTips1: '确定要直接交卷吗，',
    //提示语
    showTips2: '交卷后将直接结算当前成绩！',
    //提示语
    showTips3: ''
  },
  methods: {
    //获取数据
    getMockData: function getMockData() {
      var that = this;
      var token = wx.getStorageSync('token') || {};
      (0, _request.appRequest)(_api.mockExam, {}, 'GET', true, {
        token: token.token
      }).then(function (res) {
        if (res.flag) {
          var info = res.data || {};
          that.questionList = that.translateData(info);
          that.remainTime = (0, _utils.formatSeconds)(info.singleTime || 60); //开始倒计时

          that.countDown(that, info.singleTime || 60);
        } else {
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 1,
            mask: false
          });
        }
      });
    },
    //只获取题目数据
    getQuestionData: function getQuestionData() {
      var that = this;
      var token = wx.getStorageSync('token') || {};
      (0, _request.appRequest)(_api.mockExam, {}, 'GET', true, {
        token: token.token
      }).then(function (res) {
        if (res.flag) {
          var info = res.data || {};
          that.questionList = that.translateData(info);
          that.remainTime = (0, _utils.formatSeconds)(info.singleTime || 60); //开始倒计时
          // that.countDown(that,info.singleTime || 60)
        } else {
          $Toast({
            content: res.errMsg || '网络异常',
            type: 'error',
            duration: 1,
            mask: false
          });
        }
      });
    },
    //转化数据格式
    translateData: function translateData(info) {
      var list = [];
      var singleList = info.singleList || [];
      var multipleList = info.multipleList || [];
      var judgeList = info.judgeList || [];

      for (var i = 0; i < singleList.length; i++) {
        var item = singleList[i];
        var options = [];

        if (item.itemA) {
          options.push({
            key: "A",
            val: item.itemA,
            isSelect: false
          });
        }

        if (item.itemB) {
          options.push({
            key: "B",
            val: item.itemB,
            isSelect: false
          });
        }

        if (item.itemC) {
          options.push({
            key: "C",
            val: item.itemC,
            isSelect: false
          });
        }

        if (item.itemD) {
          options.push({
            key: "D",
            val: item.itemD,
            isSelect: false
          });
        }

        list.push({
          num: i + 1,
          type: 1,
          questionTime: info.singleTime || 60,
          typeName: '单选题',
          score: info.singleScore,
          topicContent: item.question,
          rightAnswer: item.rightAnswer,
          options: options
        });
      }

      for (var j = 0; j < multipleList.length; j++) {
        var _item = multipleList[j];
        var _options = [];

        if (_item.itemA) {
          _options.push({
            key: "A",
            val: _item.itemA,
            isSelect: false
          });
        }

        if (_item.itemB) {
          _options.push({
            key: "B",
            val: _item.itemB,
            isSelect: false
          });
        }

        if (_item.itemC) {
          _options.push({
            key: "C",
            val: _item.itemC,
            isSelect: false
          });
        }

        if (_item.itemD) {
          _options.push({
            key: "D",
            val: _item.itemD,
            isSelect: false
          });
        }

        list.push({
          num: singleList.length + 1 + j,
          type: 2,
          typeName: '多选题',
          score: info.multipleScore,
          questionTime: info.multipleTime || 60,
          topicContent: _item.question,
          rightAnswer: _item.rightAnswer,
          options: _options
        });
      }

      for (var k = 0; k < judgeList.length; k++) {
        var _item2 = judgeList[k];
        var _options2 = [];

        if (_item2.itemA) {
          _options2.push({
            key: "A",
            val: _item2.itemA,
            isSelect: false
          });
        }

        if (_item2.itemB) {
          _options2.push({
            key: "B",
            val: _item2.itemB,
            isSelect: false
          });
        }

        list.push({
          num: singleList.length + multipleList.length + 1 + k,
          type: 3,
          typeName: '判断题',
          score: info.judgeScore,
          questionTime: info.judgeTime || 60,
          topicContent: _item2.question,
          rightAnswer: _item2.rightAnswer,
          options: _options2
        });
      }

      return list;
    },
    //下一题
    next: function next() {
      var flag = false;
      var count = 0;

      for (var i = 0; i < this.questionList[this.currentNum - 1].options.length; i++) {
        if (this.questionList[this.currentNum - 1].options[i].isSelect) {
          flag = true;
          break;
        }
      }

      if (!flag && !this.disable) {
        this.showModal1 = true;
        this.showTips3 = '当前题目还未作答！';
        return;
      }

      if (this.questionList[this.currentNum - 1].type == 2) {
        for (var _i = 0; _i < this.questionList[this.currentNum - 1].options.length; _i++) {
          if (this.questionList[this.currentNum - 1].options[_i].isSelect) {
            count++;
          }
        }

        if (count <= 1 && !this.disable) {
          this.showModal1 = true;
          this.showTips3 = '当前题目为多选题，请至少选择两个选项！';
          return;
        }
      }

      this.currentNum += 1;
      console.log(this.timer, 'this.timer');
      if (this.timer) clearInterval(this.timer);
      this.remainTime = (0, _utils.formatSeconds)(this.questionList[this.currentNum - 1].questionTime || 60); //重新开始倒计时

      this.countDown(this, this.questionList[this.currentNum - 1].questionTime);
    },
    //倒计时
    countDown: function countDown(that, count) {
      that.disable = false;

      if (count <= 0) {
        that.disable = true;
        if (that.timer) clearInterval(that.timer);
        return;
      }

      that.timer = setInterval(function () {
        that.remainTime = (0, _utils.formatSeconds)(count);
        count--;

        if (count < 0) {
          that.disable = true;
          if (that.timer) clearInterval(that.timer);

          if (that.currentNum == that.questionList.length) {
            that.finish();
          } else {
            that.next();
          }

          return;
        }
      }, 1000);
    },
    //选择
    selectOption: function selectOption(item, option, index, index1) {
      var that = this; //type 1 单选 3 判断题

      if (item.type === 1 || item.type === 3) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = that.questionList[index1].options[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var question = _step.value;
            question.isSelect = false;
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
              _iterator["return"]();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }

        that.questionList[index1].options[index].isSelect = true;
      } //type 2 多选


      if (item.type === 2) {
        that.questionList[index1].options[index].isSelect = !that.questionList[index1].options[index].isSelect;
      }
    },
    //提交
    submit: function submit(flag) {
      this.showModal = true;
    },
    //关闭模态框
    cancelModal: function cancelModal() {
      this.showModal = false;
    },
    cancel: function cancel() {
      this.showModal1 = false;
    },
    //计算成绩
    calculateTotal: function calculateTotal() {
      var score = 0;

      for (var i = 0; i < this.questionList.length; i++) {
        var question = this.questionList[i]; //单选题 或 判断题

        if (question.type === 1 || question.type === 3) {
          var selectedItem = question.options.filter(function (item) {
            return item.isSelect;
          }); //选择正确即得分

          if (selectedItem && selectedItem.length != 0 && question.rightAnswer.includes(selectedItem[0].key)) {
            score += question.score;
          }
        } //多选题


        if (question.type === 2) {
          var _selectedItem = question.options.filter(function (item) {
            return item.isSelect;
          });

          var selectedItems = _selectedItem && _selectedItem.map(function (item) {
            return item.key;
          }); //选择正确即得分


          if (_selectedItem && _selectedItem.length != 0 && _utils["default"].isEquar(selectedItems, question.rightAnswer)) {
            score += question.score;
          }
        }
      }

      return score;
    },
    //确认交卷
    finish: function finish() {
      this.showModal = false;
      if (this.timer) clearInterval(this.timer); //计算成绩

      var score = this.calculateTotal();
      console.log(score, 'score');
      wx.setStorageSync('question', this.questionList);
      if (this.timer) clearInterval(this.timer); //模拟考试成绩结算

      wx.redirectTo({
        url: '/pages/achievement?id=1&score=' + score
      });
    }
  },
  onLoad: function onLoad(options) {// this.getMockData();
  },
  onShow: function onShow() {
    this.getMockData(); //重置页面

    this.currentNum = 1;
    this.remainTime = "";
    this.timer = null;
    this.disable = false;
    this.showModal = false;
    this.showModal1 = false;
  },
  onHide: function onHide() {
    if (this.timer) clearInterval(this.timer);
  },
  onUnload: function onUnload() {
    if (this.timer) clearInterval(this.timer);
  }
}, {info: {"components":{"i-button":{"path":"..\\iview\\button\\index"},"i-row":{"path":"..\\iview\\row\\index"},"i-col":{"path":"..\\iview\\col\\index"},"i-modal":{"path":"..\\iview\\modal\\index"},"i-toast":{"path":"..\\iview\\toast\\index"},"i-spin":{"path":"..\\iview\\spin\\index"}},"on":{"10-2":["tap"],"10-3":["tap"],"10-4":["ok","cancel"],"10-6":["tap"]}}, handlers: {'10-0': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.submit(1);
      })();
    
  }},'10-1': {"tap": function proxy (item, option, index, index1) {
    
    var _vm=this;
      return (function () {
        _vm.selectOption(item,option,index,index1);
      })();
    
  }},'10-2': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.next($event);
      })();
    
  }},'10-3': {"tap": function proxy () {
    
    var _vm=this;
      return (function () {
        _vm.submit(2);
      })();
    
  }},'10-4': {"ok": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.finish($event);
      })();
    
  }, "cancel": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.cancelModal($event);
      })();
    
  }},'10-6': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.cancel($event);
      })();
    
  }}}, models: {}, refs: undefined });