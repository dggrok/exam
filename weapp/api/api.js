"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.examDesc = exports.mockExam = exports.resetPwd = exports.sendCode = exports.register = exports.login = void 0;

/**
 * 登录
 */
var login = '/exam/miniapp/login';
/**
 * 账号注册
 */

exports.login = login;
var register = '/exam/miniapp/register';
/**
 * 获取验证码
 */

exports.register = register;
var sendCode = '/exam/common/sms/sendCode';
/**
 * 重置密码
 */

exports.sendCode = sendCode;
var resetPwd = '/exam/miniapp/resetPwd';
/**
 * 模拟考试
 */

exports.resetPwd = resetPwd;
var mockExam = '/exam/miniapp/mockExam';
/**
 * 正式考试
 */

exports.mockExam = mockExam;
var examDesc = '/exam/miniapp/exam/';
exports.examDesc = examDesc;