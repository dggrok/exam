"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.appRequest = appRequest;
exports.baseUrl = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var baseUrl = 'https://hzlx.exam.legalboot.com'; // export const baseUrl = 'http://172.16.14.131:8901'

/**
 * 统一的请求封装
 * @param {String} api 请求的api地址
 * @param {JSON} params 请求携带的参数
 * @param {String} methods 请求方式，默认GET
 * @param {boolean} [loading=true] 是否显示loading，默认true
 */

exports.baseUrl = baseUrl;

function appRequest(api, params) {
  var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'POST';
  var loading = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
  var header = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  return new Promise(function (resolve, reject) {
    // 请求开始 loading
    if (loading) {
      wx.showLoading({
        title: '加载中,请稍后',
        mask: true
      });
    } // 请求


    wx.request({
      url: baseUrl + api,
      data: params,
      header: _objectSpread({}, header),
      method: method,
      dataType: 'json',
      success: function success(res) {
        if (res.statusCode === 200) {
          resolve(res.data); // 成功之后的回调

          wx.hideLoading(); // 结束加载
        } else {
          wx.hideLoading();
          reject();

          if (res.data.data.errMsg === 'token已过期请重新登陆') {
            setTimeout(function () {
              wx.reLaunch({
                url: '/pages/index'
              });
            }, 2000);
          }
        }
      },
      fail: function fail(error) {
        wx.hideLoading();
        reject(error);
        wx.showToast({
          title: '网络请求异常,请稍后重试！',
          icon: 'none',
          duration: 2000
        });
      }
    });
  });
}