"use strict";

var CryptoJS = require('./../vendor.js')(38);

var Base64 = require('./../vendor.js')(4);

var key = CryptoJS.enc.Utf8.parse("cpcVEFy3rP4KwIk8"); //密钥
//解密方法

function Decrypt(word) {
  var decrypt = CryptoJS.AES.decrypt(word, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  });
  var decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
  return decryptedStr.toString();
} //加密方法


function Encrypt(word) {
  var srcs = CryptoJS.enc.Utf8.parse(word);
  var encrypted = CryptoJS.AES.encrypt(srcs, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  });
  return encrypted.ciphertext.toString(CryptoJS.enc.Base64);
}

module.exports = {
  Decrypt: Decrypt,
  Encrypt: Encrypt
};