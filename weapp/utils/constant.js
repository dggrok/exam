"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SYSTEM_INFO = exports.USER_INFO = exports.USER_SPECICAL_INFO = void 0;

/**
 * 用户code 换取 session_key
 * @type {String}
 */
var USER_SPECICAL_INFO = "userSpecialInfo";
/**
 * 用户信息
 * @type {String}
 */

exports.USER_SPECICAL_INFO = USER_SPECICAL_INFO;
var USER_INFO = "userInfo";
/**
 * 系统信息
 * @type {String}
 */

exports.USER_INFO = USER_INFO;
var SYSTEM_INFO = "systemInfo";
exports.SYSTEM_INFO = SYSTEM_INFO;