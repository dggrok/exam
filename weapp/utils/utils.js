"use strict";

function getCurrentTime() {
  var keep = '';
  var date = new Date();
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? '0' + m : m;
  var d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  var h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  var f = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
  keep = y + '' + m + '' + d + '' + h + '' + f + '' + s;
  return keep; // 20160614134947
}

function objLength(input) {
  var type = toString(input);
  var length = 0;

  if (type !== '[object Object]') {// throw '输入必须为对象{}！'
  } else {
    for (var key in input) {
      if (key !== 'number') {
        length++;
      }
    }
  }

  return length;
} // 验证是否是手机号码


function vailPhone(number) {
  var flag = true;
  var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(17[0]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/;

  if (number.length !== 11 || !myreg.test(number)) {
    flag = false;
  }

  return flag;
} // 验证是否是身份证号


function valiIdCard(string) {
  var flag = true;
  var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;

  if (!regIdNo.test(string)) {
    flag = false;
  }

  return flag;
} // 验证是否西班牙手机(6开头 9位数)


function ifSpanish(number) {
  var flag = true;
  var myreg = /^([6|7|9]{1}(\d+){8})$/;

  if (number.length !== 9 || !myreg.test(number)) {
    flag = false;
  }

  return flag;
} // 浮点型除法


function div(a, b) {
  var c, d, e, f;

  try {
    e = a.toString().split('.')[1].length;
  } catch (g) {}

  try {
    f = b.toString().split('.')[1].length;
  } catch (g) {} // [eslint] Return statement should not contain assignment. (no-return-assign)


  c = Number(a.toString().replace('.', ''));
  d = Number(b.toString().replace('.', ''));
  return mul(c / d, Math.pow(10, f - e));
} // 浮点型加法函数


function accAdd(arg1, arg2) {
  var r1, r2, m;

  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }

  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }

  m = Math.pow(10, Math.max(r1, r2));
  return ((arg1 * m + arg2 * m) / m).toFixed(2);
} // 浮点型乘法


function mul(a, b) {
  var c = 0;
  var d = a.toString();
  var e = b.toString();

  try {
    c += a.toString().split('.')[1].length;
  } catch (f) {}

  try {
    c += b.toString().split('.')[1].length;
  } catch (f) {}

  return Number(d.replace('.', '')) * Number(e.replace('.', '')) / Math.pow(10, c);
} //  遍历对象属性和值


function displayProp(obj) {
  var names = '';

  for (var name in obj) {
    names += name + obj[name];
  }

  return names;
} //  去除字符串所有空格


function sTrim(text) {
  return text.replace(/\s/g, '');
} // 去除所有:,英文冒号


function replaceColon(txt) {
  return txt.replace(/:/g, '');
} // 转换星星分数


function convertStarArray(score) {
  // 1 全星,0 空星,2半星
  var arr = [];

  for (var i = 1; i <= 5; i++) {
    if (score >= i) {
      arr.push(1);
    } else if (score > i - 1 && score < i + 1) {
      arr.push(2);
    } else {
      arr.push(0);
    }
  }

  return arr;
}
/**
 * 格式化秒
 * @param int  value 总秒数
 * @return string result 格式化后的字符串
 */


function formatSeconds(value) {
  var theTime = parseInt(value);
  var theTime1 = 0;
  var theTime2 = 0;

  if (theTime >= 60) {
    theTime1 = parseInt(theTime / 60);
    theTime = parseInt(theTime % 60);

    if (theTime1 >= 60) {
      theTime2 = parseInt(theTime1 / 60);
      theTime1 = parseInt(theTime1 % 60);
    }
  }

  if (theTime < 10) {
    theTime = '0' + parseInt(theTime);
  }

  var result = '' + theTime + '';

  if (theTime1 >= 0) {
    if (theTime1 < 10) {
      theTime1 = '0' + parseInt(theTime1);
    }

    result = '' + theTime1 + ':' + result;
  } // if(theTime2 >= 0) {
  // 	if(theTime2 < 10) {
  // 		theTime2 = "0" + parseInt(theTime2)
  // 	}
  // 	result = "" + theTime2 + ":" + result;
  // }


  return result;
}
/**
 * 判断两个数组相等
 * @param array
 * @return boolean
 */


function isEquar(a, b) {
  return JSON.stringify(a.sort()) == JSON.stringify(b.sort());
}

module.exports = {
  getCurrentTime: getCurrentTime,
  objLength: objLength,
  displayProp: displayProp,
  sTrim: sTrim,
  replaceColon: replaceColon,
  vailPhone: vailPhone,
  ifSpanish: ifSpanish,
  div: div,
  mul: mul,
  accAdd: accAdd,
  convertStarArray: convertStarArray,
  formatSeconds: formatSeconds,
  valiIdCard: valiIdCard,
  isEquar: isEquar
};